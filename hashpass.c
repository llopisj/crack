#include <stdio.h>
#include "md5.h"
#include <stdlib.h>
#include <string.h>
int main (int argc, char *argv[])
{
    FILE *fp = fopen(argv[1], "r");
    if(!fp)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
    }
    
    FILE *out = fopen(argv[2], "w");
    if(!out)
    {
        fprintf(stderr, "Can't %s for writing\n", argv[2]);
    }
    
    char line[100];
    while(fgets(line, 100, fp)!=NULL)
    {
            char *hash = md5(line, strlen(line)-1);
            fprintf(out, "%s\n", hash);
            free(hash);
    }
    
    fclose(fp);
    fclose(out);
}